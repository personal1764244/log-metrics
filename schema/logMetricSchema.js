const { HTTP_METHODS, ROUTES, PROPERTIES_TYPE } = require('../constants')

const getLogMetricsQueryParamsSchema = {
  required: ['startTime'],
  properties: ['startTime', 'endTime', 'page', 'position'],
  additionalProperties: false
}

const logMetricsReqMethodMap = {
  [ROUTES.GET_LOG_METRICS]: {
    [HTTP_METHODS.GET]: {
      queryParams: getLogMetricsQueryParamsSchema,
    }
  }
}

module.exports = {
  logMetricsReqMethodMap,
  PROPERTIES_TYPE
}

