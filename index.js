const http = require('http')
const { createRequestObject } = require('./helper/requestObjectHelper')
const handleRequests = require('./requestHandler')

const server = http.createServer(requestListener)

function requestListener(req, res) {
  const httpRequest = createRequestObject(req)

  handleRequests(httpRequest)
    .then(({ headers, statusCode, data }) => {
      res.writeHead(statusCode,  { ...headers, 'Content-Type': 'application/json' })
      res.end(data)
    })
    .catch(err => {
      console.log(err)
      res.writeHead(500, { 'Content-Type': 'application/json' })
      res.end(JSON.stringify(err))
    })
}

const port = 3000
server.listen(port, () => {
  console.log(`Server is running on port ${port}`)
})