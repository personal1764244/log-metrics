const { ROUTES } = require('./constants')
const logMetricsService = require('./service/logMetricsService')
const { validateSchema } = require('./helper/requestObjectHelper')

async function handleRequests(req) {
  const route = req.path
  switch (route) {
    case ROUTES.GET_LOG_METRICS:
      const isValid = validateSchema(req)
      if(!isValid) {
        return Promise.resolve({
          statusCode: 400,
          data: JSON.stringify({
            message: "Invalid query params"
          })
        })
      }
      const filePath = process.env.LOGS_FILE_PATH // absolute path
      if(!filePath) {
        return Promise.resolve({
          statusCode: 400,
          data: JSON.stringify({
            message: "Missing file path"
          })
        })
      }
      return await logMetricsService.getLogMetricsFromFile(req, filePath)
    default:
      return resolve({data: "Route not found", statusCode: 404, headers: { 'Content-Type': 'text/plain' }})
  }
}

module.exports = handleRequests
