const { URL } = require('url')
const { logMetricsReqMethodMap } = require('../schema/logMetricSchema')

function createRequestObject (req = {}) {
  const url = new URL(req.url, `http://${req.headers.host}`)
  const queryParams = {}
  url.searchParams.forEach((value, name) => queryParams[name] = value)
  
  return Object.freeze({
    path: url.pathname,
    method: req.method,
    queryParams: queryParams
  })
}

function validateSchema (req) {
  const { path, method, queryParams } = req
  const schema = logMetricsReqMethodMap[path]?.[method]
  if(schema) {
    const queryParamsSchema = schema.queryParams
    const isAdditionalPropertiesAllowed = queryParamsSchema.additionalProperties
    if(queryParamsSchema) {
      const requiredProperties = queryParamsSchema.required
      if(Array.isArray(requiredProperties)) {
        const areAllRequiredFieldPresent = requiredProperties.every(p => p in queryParams)
        if(!areAllRequiredFieldPresent) return false
      }

      const properties = queryParamsSchema.properties
      if(!isAdditionalPropertiesAllowed) {
        const payloadQueryParams = [...new Set(Object.keys(queryParams))]
        const isExisingProperty = payloadQueryParams.every(p => properties.includes(p))

        if(!isExisingProperty) return false
      }
    }
  }

  return true
}

module.exports = {
  createRequestObject,
  validateSchema
}