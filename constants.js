const HTTP_METHODS = {
  GET: 'GET',
  POST: 'POST'
}

const ROUTES = {
  GET_LOG_METRICS: '/logs'
}

const PROPERTIES_TYPE = {
  DATE_TIME: 'date-time',
  NUMBER: 'number'
}

module.exports = {
  HTTP_METHODS,
  ROUTES,
  PROPERTIES_TYPE
}