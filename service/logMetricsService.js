const fs = require('fs')

async function getLogMetricsFromFile(req, filePath) {
  const queryParams = req.queryParams

  const startTime = new Date(queryParams.startTime)
  const endTime = queryParams.endTime ? new Date(queryParams.endTime) : new Date()
  const logsPerPage = 100
  const position = Number(queryParams.position || 0) // last read position of the file

  return await openAndReadLogsFromFile(filePath, startTime, endTime, logsPerPage, position)
}

async function openAndReadLogsFromFile(filePath, startTime, endTime, logsPerPage, pos) {
  return new Promise((resolve, reject) => {
    fs.open(filePath, 'r', (err, fd) => {
      const bufferSize = 256
      const buffer = Buffer.alloc(bufferSize)
      const currentFp = pos

      const readChunk = (position, logs, maxSize) => {
        fs.read(fd, buffer, 0, bufferSize, position, (err, bytesRead, chunk) => {
          if (err) {
            fs.close(fd, () => {
              resolve({
                statusCode: 500,
                data: JSON.stringify({
                  err: err
                })
              })
            })

            return
          }

          const totalBytesRead = bytesRead
          let lastChunkToRead = false

          if (bytesRead === 0) {
            fs.close(fd, () => {})
            return resolve({
              statusCode: 200,
              data: JSON.stringify({
                logsPerPage,
                data: logs
              })
            })
          }

          if(position + totalBytesRead >= maxSize) lastChunkToRead = true

          const data = chunk.toString().split("\n")
          let isOutOfTimeBound = false
          /*
            While reading content, we could get incomplete line like
            2020-01-01T01:01:15.641Z Queryi
            because we are reading based on size, so we are removing
            last read line to process it properly in next iteration
          */
          if(data.length > 1) {
            let lastLine = data.pop()
            const bufferSizeOfLastLine = getBufferSizeOfContent(lastLine)

            bytesRead -= bufferSizeOfLastLine
          }

          const tLogs = []
          for (let i = 0; i <= data.length - 1; i++) {
            const line = data[i].trim()
            if (!line) continue
            
            const date = line.split(' ')[0]
            const logTime = new Date(date)

            if (logTime >= startTime && logTime <= endTime) {
              logs.push(line)
              tLogs.push(line)
            } else if(logTime > endTime) {
              isOutOfTimeBound = true
              break
            }
            
            /*
              When we have fetched maximum number of logs to be returned,
              we are reducing last line size from buffer, this is done to
              correct set fp, which is used to read file from the last
              read position
            */
            if(logs.length === logsPerPage) {
              let lastLineSize = getBufferSizeOfContent(tLogs[tLogs.length - 1])
              bytesRead =  getBufferSizeOfContent(tLogs) - lastLineSize

              break
            }
          }

          if (isOutOfTimeBound || logs.length === logsPerPage || lastChunkToRead) {
            return resolve({
              statusCode: 200,
              data: JSON.stringify({
                logsPerPage,
                data: logs,
                fp: lastChunkToRead ? 0 : position + bytesRead,
                currentFp
              })
            })
          } else {
            readChunk(position + bytesRead, logs, maxSize)
          }
        })
      }

      fs.fstat(fd, (err, stats) => {
        if (err) {
          fs.close(fd, () => {
            return reject({
              statusCode: 500,
              data: err
            })
          })
        }

        const maxSize = stats.size
        readChunk(pos, [], maxSize)
      })
    })
  })
}

function getBufferSizeOfContent(ele) {
  return Buffer.from(JSON.stringify(ele)).length
}

module.exports = {
  getLogMetricsFromFile
}
